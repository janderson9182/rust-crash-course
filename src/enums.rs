// Enums are a type which have a few definite values
pub fn run() {
    move_avatar(Movement::Up);
    move_avatar(Movement::Down);
    move_avatar(Movement::Left);
    move_avatar(Movement::Right);
}

fn move_avatar(movement: Movement) {
    match movement {
        Movement::Up => println!("Moved: UP"),
        Movement::Down => println!("Moved: DOWN"),
        Movement::Left => println!("Moved: LEFT"),
        Movement::Right => println!("Moved: RIGHT"),
    }
}

enum Movement {
    // Variants
    Up,
    Down,
    Left,
    Right
}
