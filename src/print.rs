pub fn run() {
    // print to console
    println!("Hello from the print.rs file");

    // BASIC FORMATTING
    println!("The number one aka: [{}]", 1);
    println!("{} is from {} and he's {} years old", "Brad", "Mass", 27);

    // Positional Arguments
    println!(
        "{0} is from {1} and {0} likes to {2}",
        "Brad", "Mass", "code"
    );

    // Named arguments
    println!(
        "{name} likes to play {activity}",
        name = "John",
        activity = "Baseball"
    );

    // Placeholder Traits
    println!(
        "Binary: {:b}, Hex: {:x}, Octal: {:o}",
        10, // Binary
        10, // Hex
        10 // Octal
    );

    // Placeholder for debug trait
    println!(
        "{:?}",
        (12, true, "hello")  // a tuple
    );

    println!("10 + 10 = {}", 10 + 10);
    println!("10 - 10 = {}", 10 - 10);
    println!("10 * 10 = {}", 10 * 10);
    println!("10 / 10 = {}", 10 / 10);
}
