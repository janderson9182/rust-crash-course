/*
Arrays - fixed list where the elements are the same data types.
 */

use std::mem;

pub fn run() {
    let numbers: [i32; 4] = [1, 2, 3, 4];

    // the debug syntax is {:?}
    println!("{:?}", numbers);

    // get single val
    println!("{}", numbers[0]);
    println!("{}", numbers[1]);


    // reassign a value
    let mut numbers_mutable: [i32; 5] = [1, 2, 3, 4, 5];
    numbers_mutable[2] = 20;
    println!("{:?}", numbers_mutable);

    // get array length
    println!("Length of array is {}", numbers.len());

    // arrays are stack allocated
    println!("Array occupies {} bytes",  mem::size_of_val(&numbers));

    // Get slice
    let slice: &[i32] = &numbers[1..3];

    println!("slice: {:?}", slice);
}
