// Variable s hold primitive data or references to data
// Variables are immutable bu default
// rust is a block scoped language

pub fn run() {
    let name = "Brad";
    let mut age = 37; // mut = mutable
    println!("My name is {} and I am {}", name, age);
    age = 38;
    println!("My name is {} and I am {}", name, age);

    // Define constants

    // consts normally are uppercase
    // you need to give it a type
    const ID: i32 = 001; // int 32.
    println!("ID: {}", ID);

    // assign multiple vars
    let (my_name, my_age) = ("Brad", 37);

    println!("{} is {}", my_name, my_age);
}
