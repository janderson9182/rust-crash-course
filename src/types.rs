/*
Primitive Types--

Integers:
    * u8
    * i8
    * u16
    * i16
    * u32
    * i32
    * u64
    * i64
    * u128
    * i128

number of bits they take in memory
i32 is what you mostly need
u is unsigned

Floats: f32, f64
Boolean (bool)
Characters (char)
Tuples - basically lists
Arrays - also primitive types but are a fixed length. Vectors are growable arrays.
 */

/*
    Rust is a statically typed language,
    which means that it must know the
    types of all variables at compile time.
    However, the compiler can usually infer what
    type we want to use based on the value and
    how we use it.
*/
pub fn run() {
    // default = i32
    let x = 1;

    // default = f64
    let y = 2.5;

    // add explicit type
    let z: i64 = 4546464654654654;

    // find max size
    println!("MAX i32: {}", std::i32::MAX);
    println!("MAX i64: {}", std::i64::MAX);

    // Boolean
    let is_active: bool = true;

    // Get boolean from an expression
    let is_greater_than = 10 > 5;

    // Char
    let a1 = 'a';
    let face = '\u{1f600}';// smiley face unicode. Google emoji unicode

    println!("{:?}", (x, y, z, is_active, is_greater_than, a1, face));
}
