/*
Primitive str = Immutable fixed-length string somewhere in memory
String = Growable, heap-allocated data structure - Use when you need to modify or own string data
 */
pub fn run() {
    let hello = "Hello"; // str & immutable

    let world = String::from("world");
    let length_hello = hello.len();
    let length_world = world.len();

    println!("{}(len {}), {}(len {})", hello, length_hello, world, length_world);

    let mut name = String::from("James");
    // push a char
    name.push(' ');
    // push a string
    name.push_str("Anderson");

    println!("{}", name);

    // capacity in bytes
    println!("Capacity: {}", name.capacity());

    println!("Is Empty: {}", name.is_empty());

    // contains substring
    println!("Contains the word world {}", world.contains("world"));

    // replace
    println!("Replace {}", hello.replace("Hello", "There"));

    // Loop through string by whitespace
    for word in name.split_whitespace() {
        println!("{}", word);
    }

    // Create string with capacity
    let mut s = String::with_capacity(10);
    s.push('a');
    s.push('b');
    // s.push('c');

    println!("{}", s);

    // assertion testing
    assert_eq!(2, s.len());
    assert_eq!(10, s.capacity());
}
