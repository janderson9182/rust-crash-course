
// used to create custom data types.
pub fn run() {
    let mut colour = Colour {
        red: 255,
        green: 0,
        blue: 0,
    };

    colour.red = 200;

    println!("red={}, green={}, blue={}", colour.red, colour.green, colour.blue);

    let mut color = Color(2, 4, 6);
    color.0 = 201;
    println!("red={}, green={}, blue={}", color.0, color.1, color.2);

    let mut person: Person = Person::new("James", "Anderson");
    println!("Person: {} {}", person.first_name, person.last_name);
    person.set_last_name("McAvoy");
    println!("Person full name: {}", person.full_name());
    println!("Person tuple: {:?}", person.to_tuple());
}

// Traditional Struct
struct Colour {
    red: u8,
    green: u8,
    blue: u8,
}

// Tuple Struct (US Spelling of Color)
struct Color(u8, u8, u8);

// Struct + IMPL
struct Person {
    first_name: String,
    last_name: String,
}

impl Person {
    // Construct person
    fn new(first: &str, last: &str) -> Person {
        Person {
            first_name: first.to_string(),
            last_name: last.to_string(),
        }
    }

    // Get full name
    fn full_name(&self) -> String {
        std::format!("{} {}", self.first_name, self.last_name)
    }

    // use of mut
    fn set_last_name(&mut self, new_last_name: &str) {
        self.last_name = new_last_name.to_string();
    }

    // make a tuple
    fn to_tuple(self) -> (String, String) {
        (self.first_name, self.last_name)
    }
}
