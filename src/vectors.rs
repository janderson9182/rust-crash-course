/*
Vectors - are resizable arrays
 */

use std::mem;

pub fn run() {
    let numbers: Vec<i32> = vec![1, 2, 3, 4];

    // the debug syntax is {:?}
    println!("{:?}", numbers);

    // get single val
    println!("{}", numbers[0]);
    println!("{}", numbers[1]);


    // reassign a value
    let mut numbers_mutable: Vec<i32> = vec![1, 2, 3, 4, 5];
    numbers_mutable[2] = 20;
    println!("{:?}", numbers_mutable);

    // add onto vector
    numbers_mutable.push(5);
    numbers_mutable.push(6);

    println!("{:?}", numbers_mutable);

    // pop off last
    numbers_mutable.pop();
    println!("{:?}", numbers_mutable);

    // get vector length
    println!("Length of vector is {}", numbers_mutable.len());

    // vectors are stack allocated
    println!("Array occupies {} bytes", mem::size_of_val(&numbers_mutable));

    // Get slice
    let slice: &[i32] = &numbers_mutable[1..3];

    println!("slice: {:?}", slice);

    // loop through vector values
    for x in numbers_mutable.iter() {
        println!("Number: {}", x);
    }

    // loop and mutate values
    for x in numbers_mutable.iter_mut() {
        // multiply by 2
        // this is like js map
        *x *= 2;
    }

    println!("multiplied {:?}", numbers_mutable);
}
