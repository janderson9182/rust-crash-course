pub fn run() {
    greeting("Hello", "Jane");

    // bind function values to variables

    let get_sum = add(5, 5);

    println!("Sum: {}", get_sum);

    // closure

    let n3: i32 = 10;
    let add_nums = |n1: i32, n2: i32| n1 + n2 + n3;

    println!("closure sum = {}", add_nums(8, 8))
}

fn greeting(greet: &str, name: &str) {
    println!("{} {}. Nice to meet you", greet, name)
}

fn add(n1: i32, n2: i32) -> i32 {
    // no semi colon implies return
    n1 + n2
}
