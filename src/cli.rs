pub fn run() {
    let args: Vec<String> = std::env::args().collect();
    let command = args[1].clone();

    println!("Args: {:?}", args);
    println!("First Arg: {:?}", command);

    let foo;
    match std::env::var("LOGNAME") {
        Ok(val) => foo = val,
        Err(_e) => foo = "none".to_string(),
    }

    println!("{}", foo);
}
